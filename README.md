# sisop-latihan-modul-1-2023-AM-C07

## Identitas Kelompok
| Name           | NRP        |
| ---            | ---        |
| Mashita Dewi   | 5025211036 |
| Syukra Wahyu R | 5025211037 |

## Soal 1
1. Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
    - Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking         tertinggi di Jepang.
    - Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.
    - Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
    - Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.
## Penyelesaian
Langkah pertama adalah buka file QS World University Rankings di terminal anda 
```ruby
cat '2023 QS World University Rankings.csv'
```
Langkah kedua adalah buat file bernama university_survey.sh dengan teks editor nano
```ruby
nano university_survey.sh
```
Langkah ketiga adalah lakukan perintah soal nomor 1 poin a, yaitu menampilkan 5 universitas ranking tertinggi di Jepang
```ruby
awk -F ',' '$4=="Japan" {print $2 "," $4}' '2023 QS World University Rankings.csv' | head -n 5
```
Penjelasan :
- `awk` untuk melakukan filter
- `-F ','` untuk separator dalam bentuk ','
- `$4=="Japan"` untuk memberi pernyataan bahwa filter dilakukan pada kolom keempat yang memiliki kata kunci 'Japan'
- `{print $2 "," $4}` untuk menampilkan kolom kedua yaitu nama universitas dan negaranya yang sesuai filter yang diberikan sebelumnya
- `'2023 QS World University Rankings.csv'` menandakan bahwa filter dilakukan pada file csv tersebut
- `|` sebagai separator perintah
- `head -n 5` untuk mendapatkan hasil urutan 5 teratas
 
Langkah keempat adalah lakukan perintah soal nomor 1 poin b, yaitu menampilkan FSR Score terendah dari data hasil filter nomor 1 poin a
```ruby
awk -F ',' '$4=="Japan" {print $2 "," $9}' '2023 QS World University Rankings.csv' | head -n 5 | sort -t, -k2 -r | tail -n 1 | awk -F ',' '{print $1}' 
```
Penjelasan :
- `awk -F ',' '$4=="Japan" {print $2 "," $9}' '2023 QS World University Rankings.csv' | head -n 5` melakukan perintah untuk menampilkan 5 universitas dengan FSR Score tertinggi di Jepang
- `{print $2 "," $9}'` $9 menandakan mengambil data pada kolom ke-9 yaitu FSR Score
- `sort -t,` untuk mengurutkan hasil perintah sebelumnya secara ascending
- `-k2 -r` melakukan reverse pada kolom kedua yaitu FSR Score
- `tail -n 1` mengambil satu data terendah
- `awk -F ',' '{print $1}'` menampilkan hasil dari perintah sebelumnya yaitu universitas dengan FSR Score terendah

Langkah kelima adalah lakukan perintah soal nomor 1 poin c, yaitu menampilkan 10 Universitas dengan ger tertinggi di Jepang
```ruby
awk -F ',' '$4=="Japan" {print $20 "," $2}' '2023 QS World University Rankings.csv' | sort -t, -k1 -n | head -n 10 | awk -F ',' '{print $1 "," $2}'
```
Penjelasan :
- `awk -F ',' '$4=="Japan" {print $20 "," $2}' '2023 QS World University Rankings.csv'` melakukan perintah untuk menampilkan 5 universitas dengan GER tertinggi di Jepang
- `{print $20 "," $2}'` $20 menandakan mengambil data pada kolom ke-20 yaitu GER
- `sort -t,` untuk mengurutkan hasil perintah sebelumnya secara ascending
- `-k1 -n` melakukan sort pada kolom 1 yaitu kolom GER
- `|` sebagai separator perintah
- `head -n 10` untuk mendapatkan hasil urutan 10 teratas
- `awk -F ',' '{print $1 "," $2}'` menampilkan hasil dari perintah sebelumnya yaitu universitas dengan GER tertinggi

Langkah keenam adalah lakukan perintah soal nomor 1 poin d, yaitu menampilkan universitas yang memiliki kata kunci "Keren"
```ruby
awk -F ',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
```
Penjelasan :
- `awk -F ','` untuk mengubah field separator menjadi ','
- `'/Keren/` menemukan kata kunci 'Keren'
- `{print $2} '2023 QS World University Rankings.csv'` menampilkan kolom kedua pada file csv yaitu kolom nama universitas


## Soal 2
2. Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
    - Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X         kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap         10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
        - File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
        - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat             (kumpulan_1, kumpulan_2, dst) 
    - Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip       “devil_NOMOR ZIP” dengan NOMOR.ZIPadalah urutan folder saat dibuat (devil_1, devil_2, dst). 
   
## Penyelesaian
Soal ini terdapat 2 proses penting yaitu download dan nge zip folder. Untuk itu dibuat script yang ketika di eksekusi dapat membuat 2 file tersebut.
 
1. Penjelasan file download
```ruby 
timestamp=$(date +"%H")
```

- `$(date +"%H")`berguna untuk mendapatkan waktu pada saat script di eksekusi
 ```ruby
num_folders=$(ls -d kumpulan_* | wc -l)
```
- `ls -d kumpulan_*|wc -l` berguna untuk mendapat jumlah file/direktori dengan nama kumpulan_

```ruby
dir_name="kumpulan_$((num_folders+1))"
mkdir $dir_name
```

- `dir_name`menyimpan nama direktori yang akan disimpan

- `mkdir $dir_name` membuat direktori baru dengan nama dari dir_name
```ruby
for (( i=1; i<=$downloads; i++ ))
do
    filename="perjalanan_$i.jpg"
    wget -O "$dir_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done
```
- `for (( i=1; i<=$downloads; i++ ))`merupakan perulangan/looping yang akan dijalankan dengan counter download, download itu sendiri bernilai sesuai dengan jam pada saat itu.

- `filename="perjalanan_$i.jpg"` merupakan variabel untuk jadi nama file setelah di download.


- `wget -O "$dir_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"`
berguna untuk mengunduh foto dari web yang tertera.


2. Penjelasan file ngezip
```ruby
num_zip=$(ls -d devil_* | wc -l)
```

- `ls -d devil_*|wc -l` berfungsi untuk mendapat jumlah file dengan nama devil_

- `zip_name="devil_$(($num_zip+1))"`
merupakan variabel yang menyimpan nama file setelah di zip

```ruby
for (( i=1; i<=$num_folders; i++ ))
do 
  zip -r $zip_name kumpulan_$i
  rm -r kumpulan_$i
done'
```

- `zip -r $zip_name kumpulan_$i`berguna untuk ngezip semua folder dengan nama kumpulan_* kedalam 1 zip dengan nama zip_name
- `rm -r kumpulan_$i` berguna untuk menghapus semua direktori yang telah berhasil disimpan ke dalam zip

3. Penjelasan cronjob
- `crontab -l > cronjobs` mengambil template cronjob dan di write ke dalam file dengan nama cronjobs.
```ruby 
echo "0 */10 * * * bash /home/syukra/prak/download.sh">>cronjobs
```
- `echo " ">>cronjobs` perintah menambahkan " " di akhri file cronjobs
- `0 */10 * * * bash /home/syukra/prak/download.sh`cronjob yang akan mengeksekusi file setiap 10 jam
```ruby
echo "1 0 * * * bash /home/syukra/prak/ngezip.sh">>cronjobs
```
- `1 0 * * * bash /home/syukra/prak/ngezip.sh`cronjob yang akan mengeksekusi file setiap pukul 00:01
```ruby
crontab cronjobs
```
berguna untuk menambahkan cronjobs ke dalam crontab.


## Soal 3
3. Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
    - Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
        - Minimal 8 karakter
        - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
        - Alphanumeric
        - Tidak boleh sama dengan username 
        - Tidak boleh menggunakan kata chicken atau ernie
    - Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang         dilakukan user.
        - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
        - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
        - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
        - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in


## Soal 4
4. Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. Log system tersebut harus  memiliki ketentuan : 
    - Backup file log system dengan format jam:menit tanggal:bulan:tahun.
    - Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
        - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b                diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
        - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
        - Setelah huruf z akan kembali ke huruf a
    - Backup file syslog setiap 2 jam untuk dikumpulkan 
